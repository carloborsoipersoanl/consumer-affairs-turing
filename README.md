### Installation

To the test the application:

1. Clone the project in a local server
2. Go to the folder
3. pip install -r requirements.txt 
4. python mananage.py migrate 
5. python mananage.py runserver

Install Redis and Celery. The Redis instance must run in port 6379.


PostgreSQL is configured as follows:
database name: the_eyes
user name: db_admin
user password: theeyes1234

### Test

Install httpie (https://httpie.io/)

In a command line editor, use:
http --raw '{""session_id"": ""e2085be5-9137-4e4e-80b5-f1ffddc25423"", ""category"": ""page interaction"",""name"": ""pageview"",""data"": {""host"": ""www.consumeraffairs.com"", ""path"":""//""},""timestamp"": ""2021-01-01 09:15:27.243860""}' POST http://127.0.0.1:8000/userbehavior/events/


### Requirements and use cases


It was considered that the application will be a server and not be used as a final front end.
The common use case for this application is a frontend client will access all aggregate and persistent data to 
provide a user friendly way to navigate on the results for a further analysis and decisions.


### Entities

This solution receive all events and persisted it as a raw value. Each 5 minutes, the celery application
call a function to create a final data to be used, based on the raw data. This creation will be done in 
sequence, to avoid any race conditions and remove any others processing during events receptions from sites sources.

