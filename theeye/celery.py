# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'theeye.settings') #1

app = Celery('TheEye') #2
app.config_from_object('django.conf:settings', namespace='CELERY') #3
app.autodiscover_tasks() #4

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))