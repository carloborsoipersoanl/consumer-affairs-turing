# -*- coding: utf-8 -*-
from django.utils import timezone
import json

from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed

from userbehavior.models import Event, UserSession, RawEvent
from userbehavior.permissions import IsTrustedClients


class UserSessionSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = UserSession
        fields = ['session_id']

    def create(self, request):
        raise MethodNotAllowed(method='POST')


class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RawEvent
        fields = ['category', 'name', 'data', 'timestamp', 'session_id']

        def create(self, request):
            raise MethodNotAllowed(method='POST')


class RawEventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RawEvent
        fields = ['category', 'name', 'data', 'timestamp', 'session_id']
        permission_classes = [IsTrustedClients]

        def validate(self, data):
            """
            Check that date is not in the future.
            """
            if data['timestamp'].strftime('%Y-%m-%d') > timezone.now().strftime('%Y-%m-%d'):
                raise serializers.ValidationError("timestamp is after today")
            try:
                json.loads(data['data'])
            except ValueError as e:
                raise serializers.ValidationError("data format is not a json valid format. Error: {}".format(e))

            return data


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']
