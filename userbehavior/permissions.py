# -*- coding: utf-8 -*-

from rest_framework import permissions
from rest_framework.authentication import SessionAuthentication

class IsTrustedClients(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request (GET, HEAD or OPTIONS)
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return request.user.groups.filter(name='Trusted Application').exists()
