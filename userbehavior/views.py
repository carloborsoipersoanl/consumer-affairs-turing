# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, generics

from userbehavior.models import RawEvent, UserSession, Event
from userbehavior.serializers import UserSerializer, RawEventSerializer, UserSessionSerializer, EventSerializer


class SessionListViewSet(generics.ListCreateAPIView):
    queryset = RawEvent.objects.all()
    serializer_class = RawEventSerializer


class SessionDetailViewSet(generics.RetrieveUpdateDestroyAPIView):
    queryset = UserSession.objects.all()
    serializer_class = UserSessionSerializer


class EventbyCategory(generics.ListAPIView):
    serializer_class = EventSerializer

    def get_queryset(self):
        category = self.kwargs['category']
        queryset = Event.objects.filter(category=category)
        return queryset


class EventbyDateRange(generics.ListAPIView):
    serializer_class = EventSerializer

    def get_queryset(self):
        initial_date = self.kwargs['initial_date']
        final_date = self.kwargs['final_date']
        queryset = Event.objects.filter(timestamp__gte=initial_date, timestamp__lte=final_date)
        return queryset


class RawEventViewSet(viewsets.ModelViewSet):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(RawEventViewSet, self).dispatch(*args, **kwargs)

    queryset = RawEvent.objects.all().order_by('-timestamp')
    serializer_class = RawEventSerializer
    # permission_classes = [permissions.IsAuthenticated]


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer