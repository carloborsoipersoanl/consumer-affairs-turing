# -*- coding: utf-8 -*-

from django.urls import include, path, re_path
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views

from userbehavior import views

router = routers.DefaultRouter()
router.register(r'events', views.RawEventViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),

    # Query events
    re_path(r'^sessions/$', views.SessionListViewSet.as_view()),
    re_path(r'^sessions/<int:pk>/$', views.SessionDetailViewSet.as_view()),
    re_path(r'^events_query/(?P<category>.+)/category/$', views.EventbyCategory.as_view()),
    re_path(r'^events_query/(?P<initial_date>[\w|\W]+)/(?P<final_date>[\w|\W]+)/date/$', views.EventbyDateRange.as_view()),


    # Users path
    path('users/', views.UserList.as_view()),
    path('users/<int:pk>/', views.UserDetail.as_view()),
]
