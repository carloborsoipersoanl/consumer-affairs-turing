# -*- coding: utf-8 -*-

from celery import task

from userbehavior.models import RawEvent, UserSession, Event


@task(name='update_events')
def update_events():
     events_tobe_processed = RawEvent.objects.filter(event_processed=False)
     for raw_event in events_tobe_processed:
          user_session, created = UserSession.objects.get_or_create(session_id=raw_event.session_id)
          Event.objects.create(category=raw_event.category,
                               name=raw_event.name,
                               type="{}-{}".format(raw_event.category, raw_event.name),
                               data=raw_event.data,
                               timestamp=raw_event.timestamp,
                               session=user_session)
          raw_event.event_processed = True
          raw_event.save()
