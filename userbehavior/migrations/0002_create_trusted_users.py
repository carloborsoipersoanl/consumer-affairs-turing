# -*- coding: utf-8 -*-
# Manualy crete this migration step for user group "trusted aplication"
from django.contrib.auth.management import create_permissions
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.db import migrations


def create_permission_groups(apps, schema_editor):
    for app_config in apps.get_app_configs():
        app_config.models_module = True
        create_permissions(app_config, apps=apps, verbosity=0)
        app_config.models_module = None

    pertmition_trueted = []
    models_permission = ['usersession', 'event']

    trusted_application, created = Group.objects.get_or_create(name='Trusted Application')

    print("Creation of Trusted Aplication User Group")
    for model in models_permission:
        content_type = ContentType.objects.filter(model=model).first()
        codename = "view_%s" % content_type.model
        if not Permission.objects.filter(content_type=content_type, codename=codename):
            Permission.objects.create(content_type=content_type,
                                      codename=codename,
                                      name="Can view %s" % content_type.name)
            print("Add permition for View to %s" % content_type.name)

        pertmition_trueted.append(Permission.objects.get(name='Can add %s' % content_type.name))
        pertmition_trueted.append(Permission.objects.get(name='Can delete %s' % content_type.name))
        pertmition_trueted.append(Permission.objects.get(name='Can change %s' % content_type.name))
        pertmition_trueted.append(Permission.objects.get(name='Can view %s' % content_type.name))

    trusted_application.permissions.set(pertmition_trueted)


class Migration(migrations.Migration):


    dependencies = [
        ('userbehavior', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_permission_groups),
    ]