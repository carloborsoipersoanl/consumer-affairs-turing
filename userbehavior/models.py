# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import JSONField


class UserSession(models.Model):
    session_id = models.CharField(max_length=200)

    def __str__(self):
        return self.session_id


class Event(models.Model):
    category = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    type = models.CharField(max_length=200)
    data = JSONField()
    timestamp = models.DateTimeField()
    session = models.ForeignKey(UserSession, related_name='events', on_delete=models.PROTECT)

    def __str__(self):
        return '{}-{}'.format(self.category, self.name)


class RawEvent(models.Model):
    category = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    data = JSONField()
    timestamp = models.DateTimeField()
    session_id = models.CharField(max_length=200)
    event_processed = models.BooleanField(default=False)

    def __str__(self):
        return '{}-{}'.format(self.category, self.name)
