from django.contrib import admin

# Register your models here.
from userbehavior.models import RawEvent, Event, UserSession

admin.site.register(RawEvent)
admin.site.register(Event)
admin.site.register(UserSession)
